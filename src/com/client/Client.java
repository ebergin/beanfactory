package com.client;


import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.xml.XmlBeanFactory;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import com.model.Message;

public class Client {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Resource src = new ClassPathResource("beans.xml");
		BeanFactory bf = new XmlBeanFactory(src);
		
		Object object = bf.getBean("message");
		if (object != null)
		{
			Message message = (Message)object;
			
			System.out.println(message.getMessageId() + "\t" + message.getMessage());
		}
		
		System.out.println("--------------------------");
		//same thing as above
		Message msg = bf.getBean("message",Message.class);
		System.out.println(msg.getMessageId() + "\t" + msg.getMessage());
		
		System.out.println("--------------------------");
		
		String[] aliases = bf.getAliases("message");
		for (String aName : aliases) {
			System.out.println(aName);
		}
	
		System.out.println("--------------------------");
		
		Class<?> cls = bf.getType(("message"));
		System.out.println(cls.getName());
		
	}

}
